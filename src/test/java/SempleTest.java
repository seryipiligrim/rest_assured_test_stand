import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static io.restassured.module.jsv.JsonSchemaValidator.*;

public class SempleTest {

    @Test
    public void responseJsonSchemaValidation(){
        given()
            .header("Content-Type", "application/json")
            .body("{\n" +
                "    \"id\": 12,\n" +
                "    \"category\": \"horror\",\n" +
                "    \"author\": \"Steven King\",\n" +
                "    \"title\": \"The Talisman\",\n" +
                "    \"price\": 25.1\n" +
                "}")
            .post("http://localhost:8081/library/book")
            .then()
            .assertThat()
            .body(matchesJsonSchemaInClasspath("newBook.json"));
    }

    @Test
    public void GetWeatherDetails()
    {
        // Specify the base URL to the RESTful web service
        RestAssured.baseURI = "https://api.openweathermap.org";

        // Make a request to the server by specifying the method Type and the method URL.
        // Validate Response Status
        given()
                .params("q", "Cherkasy")
                .params("APPID", "44f7de47551ae9b06bad02ee9da33ecf")
                .when()
                .get("/data/2.5/weather")
                .then()
                .statusCode(200);
    }

    @Test
    public void getBookList(){
        given()
                .get("http://localhost:8081/library/book/list")
                .then()
                .statusCode(302);
    }

    @Test
    public void getBookById(){
        given()
                .get("http://localhost:8081/library/book/3")
                .then()
                .statusCode(302);
    }

    @Test
    public void getBooksByAuthor(){
        given()
                .params("author", "'Tolstoy, Leo'")
                .get("http://localhost:8081/library/book/list")
                .then()
                .statusCode(302);
    }

    @Test
    public void getBooksByCategoryAndLimit(){
        given()
                .params("category", "fantasy")
                .params("limit", 2)
                .get("http://localhost:8081/library/book/list")
                .then()
                .statusCode(302);
    }

    @Test
    public void createNewBook(){
        given()
                .header("Content-Type", "application/json")
                .body("{\n" +
                        "    \"id\": 12,\n" +
                        "    \"category\": \"horror\",\n" +
                        "    \"author\": \"Steven King\",\n" +
                        "    \"title\": \"The Talisman\",\n" +
                        "    \"price\": 25.1\n" +
                        "}")
                .post("http://localhost:8081/library/book")
                .then()
                .statusCode(201);
    }

    @Test
    public void editBook(){
        given()
                .header("Content-Type", "application/json")
                .body("{\n" +
                        "    \"category\": \"reference\",\n" +
                        "    \"author\": \"Nigel Rees\",\n" +
                        "    \"title\": \"Sayings of the Century\",\n" +
                        "    \"price\": 11\n" +
                        "}," +
                        "")
                .put("http://localhost:8081/library/book/1")
                .then()
                .statusCode(200)
                .body("price", equalTo(11));
    }

    @Test
    public void removeBook(){
        given()
                .header("Content_Type", "application/json")
                .when()
                .delete("DELETE http://localhost:8080/library/book/11")
                .then()
                .statusCode(200);
    }
}
