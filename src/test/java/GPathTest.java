import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class GPathTest {
    @Test
    public void workWithGpathVersionOne(){
        Response response = get("http://localhost:8081/library/book/3");
        String bookName = response.path("title");
        System.out.println(bookName);
    }

    @Test
    public void workWithGpathVersionTwo(){
        Response response = get("http://localhost:8081/library/book/3");
        JsonPath jsonPath = new JsonPath(response.asString());
        String bookName = jsonPath.get("title");
        System.out.println(bookName);
    }

    @Test
    public void workWithGpathVersionThree(){
        String response = get("http://localhost:8081/library/book/3").asString();
        String bookName = JsonPath.from(response).get("title");
        System.out.println(bookName);
    }

    @Test
    public void workWithGpathVersionFour(){
        String response = get("http://localhost:8081/library/book/3").path("title");
        System.out.println(response);
    }

    @Test
    public void workWithGpathVersionFive(){
        given().
                when().
                get("http://localhost:8081/library/book/3").
                then().
                assertThat().
                body("title", equalTo("Speaking JavaScript"));
    }

    @Test
    public void extactTitleForFirstObj(){
        Response response = get("http://localhost:8081/library/book/list");
        String bookName = response.path("[0].title");
        System.out.println(bookName);
    }

    @Test
    public void extactAllObj(){
        Response response = get("http://localhost:8081/library/book/list");
        ArrayList<Map<String,?>> books = response.path(".");
        System.out.println(books);
    }

    @Test
    public void extactAllTitlesObj(){
        Response response = get("http://localhost:8081/library/book/list");
        ArrayList<String> bookNames = response.path("title");
        System.out.println(bookNames);
    }

    @Test
    public void extactSpecifiedObj(){
        Response response = get("http://localhost:8081/library/book/list");
        Map<String,?> book = response.path("find { it.title == 'The Lightning Thief' }");
        System.out.println(book);
    }

    @Test
    public void extactPriceOfSpecifiedBook(){
        Response response = get("http://localhost:8081/library/book/list");
        float price = response.path("find { it.title == 'The Lightning Thief' }.price");
        System.out.println(price);
    }

    @Test
    public void extactAllBooksWithSpecifiedCategory(){
        Response response = get("http://localhost:8081/library/book/list");
        List<String> bookTitles = response.path("findAll { it.category == 'fantasy' }.title");
        System.out.println(bookTitles);
    }

    @Test
    public void extactBookDataByComplexQueryWithParams(){
        String author = "Tolstoy, Leo";
        String title = "War and Peace";
        Response response = get("http://localhost:8081/library/book/list");
        Map<String, ?> book = response.path("findAll { it.author == '%s' }.find { it.title == '%s' }", author, title);
        System.out.println(book);
    }

    @Test
    public void findBookWithHighiestPrice(){
        Response response = get("http://localhost:8081/library/book/list");
        String bookName = response.path("max { it.price }.title");
        System.out.println(bookName);
    }

    @Test
    public void findBookWithLowestPrice(){
        Response response = get("http://localhost:8081/library/book/list");
        String bookName = response.path("min { it.price }.title");
        System.out.println(bookName);
    }

    @Test
    public void findSumOfAllPrices(){
        Response response = get("http://localhost:8081/library/book/list");
        double sumOfPrice = response.path("collect { it.price }.sum()");
        System.out.println(sumOfPrice);
    }
}
