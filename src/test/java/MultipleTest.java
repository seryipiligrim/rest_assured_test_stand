import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.core.Is.is;

public class MultipleTest {

    @BeforeClass
    public static void setupURL()
    {
        RestAssured.baseURI = "http://localhost:8081";
        RestAssured.basePath = "/bookstore/";
    }

    Creds creds = new Creds("root", "root");

    @Test
    public void multipleTest(){

        Response response = given()
                .header("Content-Type", "application/json")
                .body(creds)
                .post("login")
                .then()
                .statusCode(200)
                .extract().response();

        String token = response.path("idToken");

        given()
                .header("Content-Type", "application/json")
                .header("Authorization", token)
                .get("book/list")
                .then()
                .statusCode(302)
                .body("size()", is(10));
    }


    public String getToken(Creds credentials){
        Response response = given()
                .header("Content-Type", "application/json")
                .body(credentials)
                .post("login")
                .then()
                .statusCode(200)
                .extract().response();

        return response.path("idToken");
    }

    @Test
    public void countBoks(){
        given()
                .header("Content-Type", "application/json")
                .header("Authorization", getToken(creds))
                .get("book/list")
                .then()
                .statusCode(302)
                .body("size()", is(10));
    }
}
