import org.testng.Assert;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;

public class PojoTest {
    Book body = new Book("Dark Hill", "Mike donovan", "fantasy", 12.0);

    @Test
    public void testeSrialization(){
        given().log().all()
                .header("Content-Type", "application/json")
                .body(body)
                .post("http://localhost:8081/library/book")
                .then()
                .log().body()
                .statusCode(201);
    }

    @Test
    public void testDeserialization(){
        Book response = given()
                .get("http://localhost:8081/library/book/3").as(Book.class);

        Assert.assertEquals(response.getTitle(), "Speaking JavaScript");
    }
}