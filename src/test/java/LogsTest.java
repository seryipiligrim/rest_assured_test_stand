import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static io.restassured.module.jsv.JsonSchemaValidator.*;

public class LogsTest {

    @Test
    public void findBookWithLowestPrice(){
        Response response = given()
            .log()
            .all()
            .when()
            .get("http://localhost:8081/library/book/list");
        String bookName = response.path("min { it.price }.title");
        System.out.println(bookName);
    }

    @Test
    public void logResponseDetails(){
        given()
            .when()
            .get("http://localhost:8081/library/book/list")
            .then()
            .log()
            .everything()
            .statusCode(200);
    }

    @Test
    public void logResponseDetailsIfRequestFails(){
        given()
            .when()
            .get("http://localhost:8081/library/book/list")
            .then()
            .log()
            .ifValidationFails()
            .statusCode(300);
    }

}
