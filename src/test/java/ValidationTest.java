import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

public class ValidationTest {
    @Test
    public void bodyValidation(){
        given()
                .get("http://localhost:8081/library/book/3")
                .then()
                .body("category", equalTo("computer & IT"))
                .body("price", notNullValue());
    }

    @Test
    public void validationWithTestNG(){
        Response response = get("http://localhost:8081/library/book/list");
        float price = response.path("find { it.title == 'The Lightning Thief' }.price");

        Assert.assertEquals(price, 12.5);
    }
}
