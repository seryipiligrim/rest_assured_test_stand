import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import io.restassured.builder.*;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

public class SpecTest {

    RequestSpecification requestSpec = new RequestSpecBuilder()
    .setBaseUri("http://localhost")
    .setPort(8081)
    .addHeader("Content-Type", "application/json")
    .build();

    ResponseSpecification responseSpec = new ResponseSpecBuilder()
    .expectStatusCode(200)
    .build();

    @Test
    public void editBook(){
        given()
                .header("Content-Type", "application/json")
                .body("{\n" +
                        "    \"category\": \"reference\",\n" +
                        "    \"author\": \"Nigel Rees\",\n" +
                        "    \"title\": \"Sayings of the Century\",\n" +
                        "    \"price\": 11\n" +
                        "}")
                .put("http://localhost:8081/library/book/1")
                .then()
                .statusCode(200)
                .body("price", equalTo(11));
    }

    @Test
    public void editBookWithSpec(){
        given()
            .spec(requestSpec)
            .body("{\n" +
                    "    \"category\": \"reference\",\n" +
                    "    \"author\": \"Nigel Rees\",\n" +
                    "    \"title\": \"Sayings of the Century\",\n" +
                    "    \"price\": 11\n" +
                    "}")
            .put("/library/book/1")
            .then()
            .spec(responseSpec)
            .body("price", equalTo(11));
    }
}
