import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;


public class BaseTest {

    @BeforeClass
    public static void setupURL()
    {
        // here we setup the default URL and API base path to use throughout the tests
        RestAssured.baseURI = "http://localhost:8081";
        RestAssured.basePath = "/library/book/";
    }

    @Test
    public void checkBaseClass(){
        String response = get("3").asString();
        String bookName = JsonPath.from(response).get("title");
        System.out.println(bookName);
    }
}
